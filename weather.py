#! /usr/bin/env python3
import requests, json

DARK_SKY_SECRET_KEY="d21b17405d692b8977dd9098c59754eb"

def get_location():
    """ Returns the longitude and latitude for the location of this machine.

   Returns:

    str: longitude
    str: latitude
     """
    response = requests.get('https://ipvigilante.com/')
    json_response = response.json()
    longitude = json_response['data']['longitude']
    latitude = json_response['data']['latitude']
    return longitude, latitude
    
   
def get_temperature(longitude, latitude):
    """ Returns the current temperature at the specified location

    Parameters:
    longitude (str):
    latitude (str):

    Returns:
    float: temperature
    """
   
    r = requests.get('https://api.darksky.net/forecast/{key}/{lat},{longi}'.format(
    key=DARK_SKY_SECRET_KEY, lat=latitude, longi=longitude))
    json_r = r.json()
    temperature = json_r['currently']['temperature']
    return temperature


def print_forecast(temp):
    """ Prints the weather forecast given the specified temperature.
    Parameters:
    temp (float)
    """
    print("Today's forecast is: ", str(temp))
    
if __name__ == "__main__":
    longitude, latitude = get_location()
    print('Longitude: ', longitude)
    print('Latitude: ', latitude)

    temp = get_temperature(longitude, latitude)
    print_forecast(temp)
    
